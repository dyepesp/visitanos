package com.choucair.formacion.pageobjects;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")
public class SeccionVisitanosPage extends PageObject {

	
	//enviar la direccion
	@FindBy(id="srch-term")
	public WebElement campoBusqueda;
	
	//click en busqueda
	@FindBy(xpath="//*[@id='tab1']/div[1]/div[1]/div/button/input")
	public WebElement botonBusqueda;
	
	@FindBy(xpath="//*[@id='tab1']/div[1]/div[6]/div[1]/div/div[1]/button")
	public WebElement botonOficina;
	
	
	@FindBy(xpath="//*[@id='tab1']/div[1]/div[6]/div[1]/div/div[2]/p")
	public WebElement DireccionObtenida;
	
	

	public void enviarDireccion(String direccion) {
		campoBusqueda.clear();
		campoBusqueda.click();
		campoBusqueda.sendKeys(direccion);
		botonBusqueda.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void seleccionarOficina() {
		botonOficina.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void buscaLaDireccion() {
		String Direccion = DireccionObtenida.getText();
		enviarDireccion(Direccion);
	}

}
