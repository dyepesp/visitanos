package com.choucair.formacion.pageobjects;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")
public class SeccionPrincipalPage extends PageObject {

	// Click en Visitanos
	@FindBy(xpath = "//*[@id='footer-content']/div[1]/div/div/div[4]/div/a/img")
	public WebElement visitanos;

	public void clickEnVisitanos() {
		visitanos.click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
