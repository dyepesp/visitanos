package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.SeccionPrincipalPage;
import com.choucair.formacion.pageobjects.SeccionVisitanosPage;

import net.thucydides.core.annotations.Step;

public class SeccionVisitanosSteps {
	
	SeccionPrincipalPage seccionPrincipalPage;
	SeccionVisitanosPage seccionVisitanosPage;
	
	@Step
	public void ingresarALaWeb() {
		seccionPrincipalPage.open();
		
	}

	public void clickEnVisitanos() {
		seccionPrincipalPage.clickEnVisitanos();
	}

	public void enviarDireccion(String direccion) {
		seccionVisitanosPage.enviarDireccion(direccion);
	}

	public void seleccionarOficina() {
		seccionVisitanosPage.seleccionarOficina();
	}

	public void buscaLaDireccion() {
		seccionVisitanosPage.buscaLaDireccion();
		
	}

}
