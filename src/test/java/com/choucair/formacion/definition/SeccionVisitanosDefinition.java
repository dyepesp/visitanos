package com.choucair.formacion.definition;

import com.choucair.formacion.steps.SeccionVisitanosSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class SeccionVisitanosDefinition {
	
	@Steps
	SeccionVisitanosSteps seccionVisitanosSteps;
	
	@Given("^ingreso a la web del banco$")
	public void ingreso_a_la_web_del_banco() throws Throwable {
		seccionVisitanosSteps.ingresarALaWeb();
	}

	@Given("^dar click en visitanos$")
	public void dar_click_en_visitanos() throws Throwable {
		seccionVisitanosSteps.clickEnVisitanos();
	}
	
	@When("^ingrese la direccion \"([^\"]*)\"$")
	public void ingrese_la_direccion(String direccion) throws Throwable {
		seccionVisitanosSteps.enviarDireccion(direccion);
	}
	
	@When("^dar click en la primera oficina$")
	public void dar_click_en_la_primera_oficina() throws Throwable {
		seccionVisitanosSteps.seleccionarOficina();
	}
	
	@When("^toma la dirección de la primera oficina y la busca$")
	public void toma_la_dirección_de_la_primera_oficina_y_la_busca() throws Throwable {
		seccionVisitanosSteps.buscaLaDireccion();
	}

}
